/* Useful if you wish to make target-specific GCC changes. */
#undef TARGET_ARCHIE
#define TARGET_ARCHIE 1

/* Default arguments you want when running your
   arm-archie-gcc toolchain */
#undef LIB_SPEC
#define LIB_SPEC "-lc -larchie"

#undef TARGET_INTERWORK
#define TARGET_INTERWORK 0

#undef TARGET_CPU_DEFAULT
#define TARGET_CPU_DEFAULT "arm250"
/* Files that are linked before user code.
   The %s tells GCC to look for these files in the library directory. */
#undef STARTFILE_SPEC
#define STARTFILE_SPEC "crt0.o%s crti.o%s crtbegin.o%s"

#undef STANDARD_STARTFILE_PREFIX
#define STANDARD_STARTFILE_PREFIX "/lib/"

/* Files that are linked after user code. */
#undef ENDFILE_SPEC
#define ENDFILE_SPEC "crtend.o%s crtn.o%s crtheap.o%s"

#undef SIZE_TYPE
#define SIZE_TYPE "unsigned int"

/* Additional predefined macros. */
#undef TARGET_OS_CPP_BUILTINS
#define TARGET_OS_CPP_BUILTINS()      \
  do {                                \
    builtin_define ("__archie__");      \
    builtin_assert ("system=archie");   \
  } while(0);

#undef ASM_SPEC
#define ASM_SPEC "\
%{mlittle-endian:-EL} \
%(asm_cpu_spec) \
%{mapcs-*:-mapcs-%*} \
%(subtarget_asm_float_spec) \
%{mfloat-abi=*} %{!mfpu=auto: %{mfpu=*}} \
%(subtarget_extra_asm_spec)"
